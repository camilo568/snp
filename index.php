<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		<link href="materialize/css/materialize.min.css" type="text/css" rel="stylesheet" media="screen,projection"/>

		<title>Principal</title>
	</head>

	<body>
		<header>
			<nav>
			    <div class="nav-wrapper green darken-4">
			      <a href="../index.php" class="brand-logo">SNP</a>
			      <ul id="nav-mobile" class="right hide-on-med-and-down">
			        <li><a href="vistas/insertar.php">Insertar</a></li>
			        <li><a href="vistas/borrar.php">Borrar</a></li>
			        <li><a href="vistas/actualizar.php">Actualizar</a></li>
			        <li><a href="vistas/consultar.php">Consultar</a></li>
			        <li><a href="vistas/buscar.php">Buscar</a></li>
			      </ul>
			    </div>
			</nav>
		</header>

		<?php  
			session_start();

			if(isset($_SESSION['mensaje'])) {
				$mensaje = $_SESSION['mensaje'];
				unset($_SESSION['mensaje']);
			} 			
		?>

		<div class="center-align">
			<h2 class="light-green-text  accent-3">
			<?php  
				if(isset($mensaje)) {
					echo $mensaje;
				} else {
					echo "<br>";
				}
			?>
			</h2>
			<h1 class="green-text darken-4">Sistema Nacional de Policías (SNP)</h1>		
			<img style="max-height: 25em"  src="imagenes/police-car.jpg"  />
		</div>

			        <!--Import jQuery before materialize.js-->
	    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
	    <script type="text/javascript" src="materialize/js/materialize.min.js"></script>

	</body>
</html>