-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 19-05-2017 a las 18:11:00
-- Versión del servidor: 5.5.52-0ubuntu0.14.04.1
-- Versión de PHP: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `TrabajoBaseDeDatos1`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `administrador`
--

CREATE TABLE `administrador` (
  `cedula` int(20) NOT NULL,
  `nombre` varchar(20) NOT NULL,
  `fecha_de_nacimiento` date NOT NULL,
  `tipo_de_sangre` varchar(5) NOT NULL,
  `escuela_de_policia` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `administrador`
--

INSERT INTO `administrador` (`cedula`, `nombre`, `fecha_de_nacimiento`, `tipo_de_sangre`, `escuela_de_policia`) VALUES
(234, 'pablo', '2016-03-08', 'o+', 'una escuela de polic'),
(783, 'juan', '2017-09-21', 'b+', 'cuarta brigada');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contrato`
--

CREATE TABLE `contrato` (
  `cedadministrador` int(20) NOT NULL,
  `cedpolicia` int(20) NOT NULL,
  `codigo` int(20) NOT NULL,
  `fecha_inicio` date NOT NULL,
  `fecha_finalizacion` date NOT NULL,
  `bonificacion` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `contrato`
--

INSERT INTO `contrato` (`cedadministrador`, `cedpolicia`, `codigo`, `fecha_inicio`, `fecha_finalizacion`, `bonificacion`) VALUES
(234, 33, 883, '2017-05-15', '2017-08-17', 200);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `policia`
--

CREATE TABLE `policia` (
  `cedula` int(20) NOT NULL,
  `codigo` int(20) NOT NULL,
  `nombre` varchar(20) NOT NULL,
  `fecha_nacimiento` date NOT NULL,
  `sexo` varchar(10) NOT NULL,
  `estatura` varchar(10) NOT NULL,
  `tipo_de_sangre` varchar(5) NOT NULL,
  `salario` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `policia`
--

INSERT INTO `policia` (`cedula`, `codigo`, `nombre`, `fecha_nacimiento`, `sexo`, `estatura`, `tipo_de_sangre`, `salario`) VALUES
(33, 12, 'camilo', '2017-10-31', 'masculino', '1.79', 'b+', 5000),
(3838, 838, 'jose', '2017-05-23', 'masculino', '1.77', 'a-', 4000);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `administrador`
--
ALTER TABLE `administrador`
  ADD PRIMARY KEY (`cedula`);

--
-- Indices de la tabla `contrato`
--
ALTER TABLE `contrato`
  ADD PRIMARY KEY (`cedadministrador`,`cedpolicia`),
  ADD UNIQUE KEY `codigo` (`codigo`),
  ADD KEY `cedadministrador` (`cedadministrador`),
  ADD KEY `cedpolicia` (`cedpolicia`);

--
-- Indices de la tabla `policia`
--
ALTER TABLE `policia`
  ADD PRIMARY KEY (`cedula`),
  ADD UNIQUE KEY `codigo` (`codigo`);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `contrato`
--
ALTER TABLE `contrato`
  ADD CONSTRAINT `contrato_ibfk_1` FOREIGN KEY (`cedadministrador`) REFERENCES `administrador` (`cedula`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `contrato_ibfk_2` FOREIGN KEY (`cedpolicia`) REFERENCES `policia` (`cedula`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
