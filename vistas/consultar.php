<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		<link href="../materialize/css/materialize.min.css" type="text/css" rel="stylesheet" media="screen,projection"/>

		<title>Consultar</title>
	</head>

	<body>
		<header>
			<nav>
			    <div class="nav-wrapper green darken-4">
			      <a href="../index.php" class="brand-logo">SNP</a>
			      <ul id="nav-mobile" class="right hide-on-med-and-down">
			        <li><a href="insertar.php">Insertar</a></li>
			        <li><a href="borrar.php">Borrar</a></li>
			        <li><a href="actualizar.php">Actualizar</a></li>
			        <li><a href="consultar.php">Consultar</a></li>
			        <li><a href="buscar.php">Buscar</a></li>
			      </ul>
			    </div>
			</nav>
		</header>

		<?php  
			session_start();

			if(isset($_SESSION['mensaje'])) {
				$mensaje = $_SESSION['mensaje'];
				unset($_SESSION['mensaje']);
			}

			if(isset($_SESSION['administradores'])) {
				$administradores = $_SESSION['administradores'];
				unset($_SESSION['administradores']);
			}

			if(isset($_SESSION['policias'])) {
				$policias = $_SESSION['policias'];
				unset($_SESSION['policias']);
			}	
		?>

		<div class="center-align container">
			<h2 class="light-green-text  accent-3">
			<?php  
				if(isset($mensaje)) {
					echo $mensaje;
				} else {
					echo "<br>";
				}
			?>
			</h2>	

			<h2 class="green-text darken-4">Seleccione una opción</h2>
			<div class="row">				
				<a href="../conexion/consultarAdministradores.php" class="waves-effect waves-light btn-large">consultar Administrador</a>
				<a href="../conexion/consultarPolicias.php" class="waves-effect waves-light btn-large">consultar Policia</a>
			</div>		

			<?php if (isset($administradores)) {?>
				<h2 class="grey-text">Administradores</h2>
				<table class="striped">
				    <thead>
	         			<tr>
						<?php	foreach ($administradores[0] as $atributo => $valor ) {?>
							<th><?php	echo $atributo; ?></th>
						<?php } ?>
						</tr>					
					</thead>
					<tbody>
					<?php foreach ($administradores as $key => $administrador) {?>					
	          			<tr>
						<?php	foreach ($administrador as $atributo => $valor ) {?>
							<td><?php	echo $valor; ?></td>
						<?php } ?>
						</tr>
					<?php } ?>
	        		</tbody>
	      		</table>
	      	<?php } ?>

	      	<?php if (isset($policias)) {?>
				<h2 class="grey-text">Policias</h2>
				<table class="striped">
				    <thead>
	         			<tr>
						<?php	foreach ($policias[0] as $atributo => $valor ) {?>
							<th><?php	echo $atributo; ?></th>
						<?php } ?>
						</tr>					
					</thead>
					<tbody>
					<?php foreach ($policias as $key => $policia) {?>					
	          			<tr>
						<?php	foreach ($policia as $atributo => $valor ) {?>
							<td><?php	echo $valor; ?></td>
						<?php } ?>
						</tr>
					<?php } ?>
	        		</tbody>
	      		</table>
	      	<?php } ?>
      	</div>
			        <!--Import jQuery before materialize.js-->
	    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
	    <script type="text/javascript" src="../materialize/js/materialize.min.js"></script>

	</body>
</html>