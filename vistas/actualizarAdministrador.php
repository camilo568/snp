<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		<link href="../materialize/css/materialize.min.css" type="text/css" rel="stylesheet" media="screen,projection"/>

		<title>Actualizar</title>
	</head>

	<body>
		<header>
			<nav>
			    <div class="nav-wrapper green darken-4">
			      <a href="../index.php" class="brand-logo">SNP</a>
			      <ul id="nav-mobile" class="right hide-on-med-and-down">
			        <li><a href="insertar.php">Insertar</a></li>
			        <li><a href="borrar.php">Borrar</a></li>
			        <li><a href="actualizar.php">Actualizar</a></li>
			        <li><a href="consultar.php">Consultar</a></li>
			        <li><a href="buscar.php">Buscar</a></li>
			      </ul>
			    </div>
			</nav>
		</header>

		<?php  
			session_start();

			if(isset($_SESSION['mensaje'])) {
				$mensaje = $_SESSION['mensaje'];
				unset($_SESSION['mensaje']);
			} 

			if (isset($_SESSION['administrador'])) {
				$administrador = $_SESSION['administrador'];
				unset($_SESSION['administrador']);
			}			
		?>

		<div class="center-align container">
			<h2 class="light-green-text  accent-3">
			<?php  
				if(isset($mensaje)) {
					echo $mensaje;
				} else {
					echo "<br>";
				}
			?>
			</h2>			
			<div class="card-panel  green lighten-5">
			
				<h2 class="grey-text">Actualizar administrador</h2>
				<form role="form" name="actualizarAdministrador" action="../conexion/actualizarAdministrador.php" method="post">
				  	<div class="row">
						    <div class="input-field col s6">
						    	<input readonly id="cedula" name="cedula" type="number" class="" 
						    	value="<?php if (isset($administrador['cedula'])) {echo $administrador['cedula'];}?>">
						    	<label class="active" for="cedula">Cedula</label>
						    </div>
					</div>
					<div class="row">
						    <div class="input-field col s6">
						    	<input id="nombre" name="nombre" type="text" class="validate" 
						    	value="<?php if (isset($administrador['nombre'])) {echo $administrador['nombre'];}?>">
						    	<label class="active" for="nombre">Nombre</label>
						    </div>
					</div>
					<div class="row">
						    <div class="input-field col s6">
						    	<input id="fecha_de_nacimiento" name="fecha_de_nacimiento" type="date" class="datepicker" 
						    	value="<?php if (isset($administrador['fecha_de_nacimiento'])) {echo $administrador['fecha_de_nacimiento'];}?>">
						    	<label class="active" for="fecha_de_nacimiento">Fecha de nacimiento</label>
						    </div>
					</div>
					<div class="row">
						    <div class="input-field col s6">
						    	<input id="tipo_de_sangre" name="tipo_de_sangre" type="text" class="validate" 
						    	value="<?php if (isset($administrador['tipo_de_sangre'])) {echo $administrador['tipo_de_sangre'];}?>">
						    	<label class="active" for="tipo_de_sangre">Tipo de sangre</label>
						    </div>
					</div>
					<div class="row">
						    <div class="input-field col s6">
						    	<input id="escuela_de_policia" name="escuela_de_policia" type="text" class="validate" 
						    	value="<?php if (isset($administrador['escuela_de_policia'])) {echo $administrador['escuela_de_policia'];}?>">
						    	<label class="active" for="escuela_de_policia">Escuela de policía</label>
						    </div>
					</div>
				  	<button type="submit" class="waves-effect waves-light btn">Actualizar</button>
				</form>
			</div>			
		</div>
			        <!--Import jQuery before materialize.js-->
	    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
	    <script type="text/javascript" src="../materialize/js/materialize.min.js"></script>
	</body>
</html>
<script type="text/javascript">
	$('.datepicker').pickadate({
	selectMonths: true, // Creates a dropdown to control month
	selectYears: 15, // Creates a dropdown of 15 years to control year
	format: 'yyyy-mm-dd'
	});
</script>