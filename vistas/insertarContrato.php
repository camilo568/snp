<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		<link href="../materialize/css/materialize.min.css" type="text/css" rel="stylesheet" media="screen,projection"/>

		<title>Insertar</title>
	</head>

	<body>
		<header>
			<nav>
			    <div class="nav-wrapper green darken-4">
			      <a href="../index.php" class="brand-logo">SNP</a>
			      <ul id="nav-mobile" class="right hide-on-med-and-down">
			        <li><a href="insertar.php">Insertar</a></li>
			        <li><a href="borrar.php">Borrar</a></li>
			        <li><a href="actualizar.php">Actualizar</a></li>
			        <li><a href="consultar.php">Consultar</a></li>
			        <li><a href="buscar.php">Buscar</a></li>
			      </ul>
			    </div>
			</nav>
		</header>

		<?php  
			session_start();

			if(isset($_SESSION['mensaje'])) {
				$mensaje = $_SESSION['mensaje'];
				unset($_SESSION['mensaje']);
			} 

			if (isset($_SESSION['indentificadoresAdministradores'])) {
				$indentificadoresAdministradores = $_SESSION['indentificadoresAdministradores'];
				unset($_SESSION['indentificadoresAdministradores']);
			}

			if (isset($_SESSION['indentificadoresPolicias'])) {
				$indentificadoresPolicias = $_SESSION['indentificadoresPolicias'];
				unset($_SESSION['indentificadoresPolicias']);
			}

			if (isset($_SESSION['formulario'])) {
				$formulario = $_SESSION['formulario'];
				unset($_SESSION['formulario']);
			}			
		?>

		<div class="center-align container">
			<h2 class="light-green-text  accent-3">
			<?php  
				if(isset($mensaje)) {
					echo $mensaje;
				} else {
					echo "<br>";
				}
			?>
			</h2>			
			<div class="card-panel  green lighten-5">
			
				<h2 class="grey-text">Insertar contrato</h2>
				<form role="form" name="insertarContrato" action="../conexion/insertarContrato.php" method="post">
					<div class="row">
						    <div class="input-field col s6">
								<select name="cedadministrador"  type="number">
									<option value="" disabled selected>Escoja la cedula del administrador</option>
								    <?php foreach ($indentificadoresAdministradores as $key => $indentificador) {  ?> 
								    	<option value= <?php echo $indentificador['cedula']; ?> > <?php echo $indentificador['cedula']; ?> </option> 
								    <?php } ?>			
								</select>
								<label>Cedula administrador</label>
							</div>
					</div>
					<div class="row">
						    <div class="input-field col s6">
								<select name="cedpolicia">
									<option value="" disabled selected>Escoja la cedula del policia</option>
								    <?php foreach ($indentificadoresPolicias as $key => $indentificador) {  ?>
								    	<option value= <?php echo $indentificador['cedula']; ?> > <?php echo $indentificador['cedula']; ?> </option>
								    <?php } ?>			
								</select>
								<label>Cedula policia</label>
							</div>
					</div>
				  	<div class="row">
						    <div class="input-field col s6">
						    	<input id="codigo" name="codigo" type="number" class="validate" 
						    	value="<?php if (isset($formulario['codigo'])) {echo $formulario['codigo'];}?>">
						    	<label class="active" for="codigo">Codigo</label>
						    </div>
					</div>
					<div class="row">
						    <div class="input-field col s6">
						    	<input id="fecha_inicio" name="fecha_inicio" type="date" class="datepicker"
						    	value="<?php if (isset($formulario['fecha_inicio'])) {echo $formulario['fecha_inicio'];}?>">
						    	<label class="active" for="fecha_inicio">Fecha de inicio</label>
						    </div>
					</div>
					<div class="row">
						    <div class="input-field col s6">
						    	<input id="fecha_finalizacion" name="fecha_finalizacion" type="date" class="datepicker" 
						    	value="<?php if (isset($formulario['fecha_finalizacion'])) {echo $formulario['fecha_finalizacion'];}?>">
						    	<label class="active" for="fecha_finalizacion">Fecha de finalizacion</label>
						    </div>
					</div>  
					<div class="row">
						    <div class="input-field col s6">
						    	<input id="bonificacion" name="bonificacion" type="number" class="validate" 
						    	value="<?php if (isset($formulario['bonificacion'])) {echo $formulario['bonificacion'];}?>">
						    	<label class="active" for="bonificacion">Bonificación</label>
						    </div>
					</div>
				  	<button type="submit" class="waves-effect waves-light btn">Insertar</button>
				</form>
			</div>			
		</div>
			        <!--Import jQuery before materialize.js-->
	    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
	    <script type="text/javascript" src="../materialize/js/materialize.min.js"></script>
	</body>
</html>
<script type="text/javascript">
	$(document).ready(function() {
	$('select').material_select();
	});

	$('.datepicker').pickadate({
	selectMonths: true, // Creates a dropdown to control month
	selectYears: 15, // Creates a dropdown of 15 years to control year
	format: 'yyyy-mm-dd'
	});
</script>