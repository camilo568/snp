<?php
	include "db.php";		
	
	// $sql = "select * from administrador where codigo = $_POST[codigo]";
	$sql = "SELECT * 
			FROM administrador 
			WHERE  cedula NOT IN (SELECT cedadministrador 
									FROM 	contrato )";
	$result = $db->query($sql);

	session_start();

	if ($result->num_rows != 0) { 
		$administradores =  $result->fetch_all(MYSQLI_ASSOC);
		$_SESSION['administradores'] = $administradores ;
		header('Location: ../vistas/consultar.php'); 
	} else {
		$mensaje = "No existen administradores que no tengan contratos";
		$_SESSION['mensaje'] = $mensaje;
		header('Location: ../vistas/consultar.php'); 
	}

?>
