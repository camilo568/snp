<?php
	include "db.php";		
		
	$sql = "DELETE FROM administrador WHERE cedula = $_POST[cedula]";
	
	$db->query($sql);
	session_start();

	if ($db->affected_rows == 0) { 
		$mensaje = "El administrador no existe";
		$_SESSION['mensaje'] = $mensaje;
		$_SESSION['formulario'] = ["cedula" => "$_POST[cedula]"];
		header('Location: ../vistas/cargarBorrarAdministrador.php');
	} else {
		$mensaje = "Se borro el administrador con cedula $_POST[cedula]";
		$_SESSION['mensaje'] = $mensaje;
		header('Location: ../index.php');		
	}
?>