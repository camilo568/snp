<?php
	include "db.php";		
		
	$sql = "SELECT contrato.cedadministrador,
					administrador.nombre
			FROM contrato
			JOIN administrador
				ON contrato.cedadministrador = administrador.cedula 
			WHERE cedpolicia = $_POST[cedula]";	
	$resultAdministradores = $db->query($sql);



	$sql = "SELECT (salario + COALESCE(SUM( bonificacion), 0)) AS ganancia_total
			FROM policia
			LEFT JOIN contrato  
				ON contrato.cedpolicia = policia.cedula				
			WHERE cedula = $_POST[cedula]
			GROUP BY cedpolicia";	
	$resultGanancias = $db->query($sql);	

	session_start();

	if ($resultGanancias->num_rows != 0) { 
		$gananciaPolicia =  $resultGanancias->fetch_all(MYSQLI_ASSOC);
		$_SESSION['gananciaPolicia'] = $gananciaPolicia ;		
	} else {
		$mensaje = "El policia ingresado no existe";
		$_SESSION['mensaje'] = $mensaje; 
	}

	if ($resultAdministradores->num_rows != 0) { 
		$administradores =  $resultAdministradores->fetch_all(MYSQLI_ASSOC);
		$_SESSION['administradores'] = $administradores ;		
	} else {
		$mensaje .= "<br> No exiten contratos relacionados con el policia";
		$_SESSION['mensaje'] = $mensaje; 
	}

	header('Location: ../vistas/buscar.php'); 
?>