<?php
	include "db.php";		
	
	$sql = "SELECT cedula  FROM policia";		
	$result = $db->query($sql);	
	$indentificadores =  $result->fetch_all(MYSQLI_ASSOC);
	
	session_start();	

	if (isset($indentificadores)) { 		
		$_SESSION['indentificadores'] = $indentificadores ;
		header('Location: ../vistas/seleccionarPolicia.php');
	} else {
		$_SESSION['mensaje'] = "No existen policias insertados";
		header('Location: ../vistas/actualizar.php');
	}	
?>
