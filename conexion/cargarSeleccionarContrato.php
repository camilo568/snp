<?php
	include "db.php";		

	$sql = "SELECT cedula  FROM administrador";		
	$result = $db->query($sql);	
	$indentificadoresAdministradores =  $result->fetch_all(MYSQLI_ASSOC);

	$sql = "SELECT cedula  FROM policia";	
	$result = $db->query($sql);	
	$indentificadoresPolicias =  $result->fetch_all(MYSQLI_ASSOC);

	session_start();

	if (isset($indentificadoresAdministradores) and isset($indentificadoresPolicias)) { 		
		$_SESSION['indentificadoresAdministradores'] = $indentificadoresAdministradores ;
		$_SESSION['indentificadoresPolicias'] = $indentificadoresPolicias ;
		header('Location: ../vistas/seleccionarContrato.php');
	} else {
		$_SESSION['mensaje'] = "No existen administradores o policias insertados";
		header('Location: ../vistas/actualizar.php');
	}		
?>
