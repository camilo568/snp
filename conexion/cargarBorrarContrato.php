<?php
	include "db.php";		
	
	$sql = "SELECT cedula  FROM administrador";		
	$result = $db->query($sql);	
	$indentificadoresAdministradores =  $result->fetch_all(MYSQLI_ASSOC);

	$sql = "SELECT cedula  FROM policia";	
	$result = $db->query($sql);	
	$indentificadoresPolicias =  $result->fetch_all(MYSQLI_ASSOC);
// acá se pone lo de identificador alternativo? en la base de datos se pone unique
	session_start();

	if (isset($indentificadoresAdministradores) and isset($indentificadoresPolicias)) { 		
		$_SESSION['indentificadoresAdministradores'] = $indentificadoresAdministradores ;
		$_SESSION['indentificadoresPolicias'] = $indentificadoresPolicias ;
		header('Location: ../vistas/borrarContrato.php');
	} else {
		$_SESSION['mensaje'] = "No existen administradores o policias insertados";
		header('Location: ../vistas/borrar.php');
	}	
?>
