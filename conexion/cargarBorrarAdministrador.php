<?php
	include "db.php";		
	
	$sql = "SELECT cedula FROM `administrador`";
	$result = $db->query($sql);	
	$indentificadores =  $result->fetch_all(MYSQLI_ASSOC);
	
	session_start();	

	if (isset($indentificadores)) { 		
		$_SESSION['indentificadores'] = $indentificadores ;
		header('Location: ../vistas/borrarAdministrador.php');
	} else {
		$_SESSION['mensaje'] = "No existen administradores insertados";
		header('Location: ../vistas/borrar.php');
	}	
?>
