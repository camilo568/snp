<?php
	include "db.php";		
		
	$sql = "DELETE FROM policia WHERE cedula = $_POST[cedula]";	
	$db->query($sql);
	
	session_start();

	if ($db->affected_rows == 0) { 
		$mensaje = "El policia no existe";
		$_SESSION['mensaje'] = $mensaje;
		$_SESSION['formulario'] = ["cedula" => "$_POST[cedula]"];
		header('Location: ../vistas/cargarBorrarPolicia.php');
	} else {
		$mensaje = "Se borro el policia con cedula $_POST[cedula]";
		$_SESSION['mensaje'] = $mensaje;
		header('Location: ../index.php');		
	}
?>