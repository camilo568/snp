<?php
	include "db.php";		
	
	$sql = "INSERT INTO administrador(cedula,
									nombre,
									fecha_de_nacimiento,
									tipo_de_sangre,
									escuela_de_policia)
							VALUES ('$_POST[cedula]',
									'$_POST[nombre]',
									'$_POST[fecha_de_nacimiento]',
									'$_POST[tipo_de_sangre]',
									'$_POST[escuela_de_policia]') ";	
	$result = $db->query($sql);
	
	session_start();

	if ($result) { 
		$mensaje = "Administrador insertado";
		$_SESSION['mensaje'] = $mensaje;
		header('Location: ../index.php');
	} else {
		$mensaje = $db->error;
		$_SESSION['mensaje'] = $mensaje;
		$_SESSION['formulario'] = ["cedula" => "$_POST[cedula]",
								   "nombre" => "$_POST[nombre]",
								   "fecha_de_nacimiento" => "$_POST[fecha_de_nacimiento]",
								   "tipo_de_sangre" => "$_POST[tipo_de_sangre]",
								   "escuela_de_policia" => "$_POST[escuela_de_policia]"];
		header('Location: ../vistas/insertarAdministrador.php');
	}	 
?>