<?php
	include "db.php";	

	$sql = "SELECT policia.cedula,
					policia.codigo,
					policia.nombre,
					policia.fecha_nacimiento,
					policia.sexo,
					policia.estatura,
					policia.tipo_de_sangre,
					policia.salario
			FROM policia
			LEFT JOIN contrato
				ON 	contrato.cedpolicia = policia.cedula   
			GROUP BY policia.cedula
			HAVING COUNT(cedpolicia) < 4";

	$result = $db->query($sql);

	session_start();

	if ($result->num_rows != 0) { 
		$policias =  $result->fetch_all(MYSQLI_ASSOC);
		$_SESSION['policias'] = $policias ;
		header('Location: ../vistas/consultar.php'); 
	} else {
		$mensaje = "No existen policias que tengan menos de 4 contratos";
		$_SESSION['mensaje'] = $mensaje;
		header('Location: ../vistas/consultar.php'); 
	}

?>
