<?php
	include "db.php";		
	
	$sql = "SELECT * FROM administrador WHERE cedula = $_POST[cedula]";		

	$result = $db->query($sql);
	$administrador =  $result->fetch_array(MYSQLI_ASSOC);
	session_start();	

	if (isset($administrador)) { 		
		$_SESSION['administrador'] = $administrador ;
		header('Location: ../vistas/actualizarAdministrador.php');
	} else {
		$_SESSION['mensaje'] = "El administrador no ha sido insertado";
		header('Location: ../vistas/actualizar.php');
	}	
?>
